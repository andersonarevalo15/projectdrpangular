import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './lib/font-awesome/css/font-awesome.min.css','./lib/animate/animate.min.css',
  './lib/ionicons/css/ionicons.min.css', './lib/owlcarousel/assets/owl.carousel.min.css', './lib/lightbox/css/lightbox.min.css',
  './lib/bootstrap/css/bootstrap.min.css']
})
export class AppComponent {
  title = 'ProjectDrpAngular';
}
